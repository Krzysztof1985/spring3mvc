<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <script type="text/javascript" src="jquery.js"></script> -->
<!-- <script type="text/javascript" src="skrypt1.js"></script> -->
<script src="<c:url value="./jquery.js" />"></script>
<script src="<c:url value="./skrypt1.js"/>"></script>
<title>Spring 3 MVC and Hibernate Example</title>
</head>
<body>
	<h1>Spring 3 MVC and Hibernate Example</h1>

	<a href="articles.html">List of Articles</a>
	<br />
	<a href="articles/add.html">Add Article</a>
	<br />
	<a href="test.html">TEST</a>
	<a href="login.html">LOGIN</a>
	<p>
		<input type="button" id="testMe" value="TEST JQUERY">
	</p>
</body>
</html>