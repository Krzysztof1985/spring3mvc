<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="<c:url value="/./jquery.js" />"></script>
<script src="<c:url value="/./skrypt1.js" />"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
	width: 120px;
}
</style>
</head>
<body>
	<c:url var="viewArticlesUrl" value="/articles.html" />
	<a href="${viewArticlesUrl}">Show All Articles</a>
	<form:form action="update.html" method="POST" modelAttribute="article">
		<form:errors path="*" cssClass="errorblock" element="div" />
		<table>
			<tr>
				<td>Id</td>
				<td><form:input path="articleId" readonly="true" />
			</tr>
			<tr>
				<td>Article Name</td>
				<td><form:input path="articleName" /> <%-- 				<form:errors path="articleName" /> --%>
			</tr>
			<tr>
				<td>Article Desc</td>
				<td><form:input path="articleDesc" /> <%-- 					<form:errors path="articleDesc" /> --%>
			</tr>
			<tr>
				<td>Date created</td>
				<td><form:input path="addedDate" />
			</tr>
		</table>
		${msg}
		<input type="submit" value="SAVE" />
	</form:form>
	<a href="<c:url value="j_spring_security_logout" />"> Logout</a>
</body>
</html>