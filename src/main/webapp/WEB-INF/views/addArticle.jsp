<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<script src="<c:url value="/./jquery.js" />"></script>
<script src="<c:url value="/./skrypt1.js" />"></script>
<title>Add Article</title>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
	width: 120px;
}
</style>
</head>
<body>
	<h1>Add Article</h1>
	<c:url var="viewArticlesUrl" value="/articles.html" />
	<a href="${viewArticlesUrl}">Show All Articles</a>
	<br></br>
	<c:url var="saveArticleUrl" value="/articles/save.html" />
	<form:form modelAttribute="article" method="POST"
		action="${saveArticleUrl}">
		<form:errors path="*" cssClass="errorblock" element="div" />
		<form:label path="articleName">Article Name:</form:label>
		<form:input path="articleName" size="30" />
		<br></br>
		<form:label path="articleDesc">Article Description:</form:label>
		<form:textarea path="articleDesc" rows="3" cols="40" />
		<br />
		<br />
		<input type="submit" value="Save Article" />
	</form:form>
	<a href="<c:url value="/j_spring_security_logout" />"> Logout</a>
</body>
</html>