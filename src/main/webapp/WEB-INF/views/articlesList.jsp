<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>All Articles</title>
<script type="text/javascript">
	function deleteItem() {
		var status = confirm("Do you really want to delete this?");
		if (status) {
			return true;
		} else
			return false;
	}
</script>
<script src="<c:url value="/./jquery.js" />"></script>
<script src="<c:url value="/./skrypt1.js" />"></script>
</head>
<body>
	<h1>List Articles</h1>
	<a href="articles/add.html">Add Article</a>
	<br></br>
	<div>
		<c:url var="viewArticlesUrl" value="/articles.html" />
		<a href="${viewArticlesUrl}">Show All Articles</a><br /> ${message}
	</div>
	<c:if test="${!empty articles}">
		<table border="5" width="600">
			<tr>
				<th>Article ID</th>
				<th>Article Name</th>
				<th>Article Desc</th>
				<th>Added Date</th>
				<th>EDIT</th>
				<th>DELETE</th>
			</tr>

			<c:forEach items="${articles}" var="article">
				<tr>
					<td><c:out value="${article.articleId}" /></td>
					<td><c:out value="${article.articleName}" /></td>
					<td><c:out value="${article.articleDesc}" /></td>
					<td><fmt:formatDate pattern="dd/MM/yyyy" value="${article.addedDate}" /></td>
					<td><a href="update.html?id=${article.articleId}">Edit</a></td>
					<td><form:form method="POST"
							action="deleteArticle.html?uid=${article.articleId}">
							<input type="submit" id="btnSubmit" name="btnSubmit"
								value="Delete" onclick="return deleteItem();" />
						</form:form></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
	<a href="<c:url value="j_spring_security_logout" />"> Logout</a>
	<p>
		<input type="button" id="testMe" value="TEST JQUERY">
	</p>
</body>
</html>