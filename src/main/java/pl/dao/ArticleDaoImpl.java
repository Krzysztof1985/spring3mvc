package pl.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.model.Article;

@Repository("articleRepository")
public class ArticleDaoImpl implements ArticleDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Article getArticleById(Long artID) {
		Article article = (Article) getSession().get(Article.class, artID);
		return article;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void deleteArticle(Long artID) {
		Article art = getArticleById(artID);
		getSession().delete(art);
	}

	@Override
	public void saveArticle(Article article) {
		article.setAddedDate(new Date());
		getSession().save(article);
	}

	@Override
	public List<Article> articleList() {
		List<Article> list = getSession().createCriteria(Article.class).list();
		System.out.println("Lista ma rozmiar " + list.size());
		return list;
	}

	@Override
	public void saveUpdatedArt(Article article) {
		getSession().saveOrUpdate(article);
	}
}
