package pl.dao;

import java.util.List;

import pl.model.Article;

public interface ArticleDAO {

	public Article getArticleById(Long artID);

	public void deleteArticle(Long artID);

	public void saveArticle(Article article);

	public List<Article> articleList();
	
	public void saveUpdatedArt(Article article);
}
