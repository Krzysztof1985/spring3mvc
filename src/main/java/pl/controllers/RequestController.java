package pl.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.model.Article;
import pl.service.ArticleService;

@Controller
public class RequestController {

	private static final Logger logger = LoggerFactory.getLogger(RequestController.class);

	@Autowired
	private ArticleService articleService;

	@RequestMapping(value = "/articles/add", method = RequestMethod.GET)
	public String addArticle(@ModelAttribute("article") Article article, ModelMap model) {
		model.addAttribute("article", new Article());
		logger.debug("Zalogowano  "+model.toString());
		System.out.println("Model -->" + model.toString());
		return "addArticle";
	}

	@RequestMapping(value = "/articles/save", method = RequestMethod.POST)
	public ModelAndView saveArticle(@ModelAttribute("article") @Valid Article article, BindingResult result) {
		if (result.hasErrors()) {
		logger.debug("KURDE BLEDY SA");
			// return new ModelAndView("addArticle");
			return new ModelAndView("addArticle");
		}
		articleService.dodajArtykul(article);
		return new ModelAndView("redirect:/articles.html");
	}
	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView editArticle(@RequestParam("id") Long uid) {

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("article", articleService.getArticleByID(uid));
		return new ModelAndView("editArticle", model);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView saveUpdatedArticle(@ModelAttribute("article") @Valid Article article, BindingResult result) {
		System.out.println("Czy sa bledy " + result.hasErrors());
		if (result.hasErrors()) {
			return new ModelAndView("editArticle");
		}
		System.out.println(article.toString());
		System.out.println("Nullowa data " + article.getAddedDate());

		if (article.getAddedDate() == null) {
			article.setAddedDate(new Date());
		}
		articleService.saveUpdated(article);
		return new ModelAndView("redirect:/articles.html");
	}

	@RequestMapping(value = "/articles", method = RequestMethod.GET)
	public ModelAndView listArticles() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("articles", articleService.listOfArticles());
		return new ModelAndView("articlesList", model);
	}

	@RequestMapping(value = "/deleteArticle", method = RequestMethod.POST)
	public ModelAndView deleteArticlePOST(@RequestParam("uid") Long nrID) {
		String message = "Usunieto rekord!";
		articleService.deleteArticle(nrID);
		return new ModelAndView("articlesList", "message", message);
	}

	@RequestMapping(value = "/deleteArticle", method = RequestMethod.GET)
	public ModelAndView deleteArticleGET(@RequestParam("uid") Long nrID) {
		System.out.println("Probujesz wbic sie tutaj getem co jest zabronione!!!!");
		return new ModelAndView("redirect:/articles.html");
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ModelAndView showTestPage() {
		return new ModelAndView("test");
	}
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
}
