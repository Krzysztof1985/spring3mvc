package pl.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller
public class BasicController {

	private static final Logger logger = LoggerFactory.getLogger(BasicController.class);

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String helloPage() {
		logger.info("Enter to home controller");
		return "test";
	}
}
