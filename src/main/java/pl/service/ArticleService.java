package pl.service;

import java.util.List;

import pl.model.Article;

public interface ArticleService {
	public void dodajArtykul(Article article);
	public List<Article> listOfArticles();
	public void deleteArticle(Long id);
	public Article getArticleByID(Long Id);

	public void saveUpdated(Article article);
}
