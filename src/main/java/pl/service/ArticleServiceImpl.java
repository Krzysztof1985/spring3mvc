package pl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pl.dao.ArticleDAO;
import pl.model.Article;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, isolation = Isolation.REPEATABLE_READ)
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private ArticleDAO articleDAO;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void dodajArtykul(Article article) {
		articleDAO.saveArticle(article);
	}

	@Override
	public List<Article> listOfArticles() {
		return articleDAO.articleList();
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void deleteArticle(Long id) {
		articleDAO.deleteArticle(id);
	}

	@Override
	public Article getArticleByID(Long Id) {
		return articleDAO.getArticleById(Id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveUpdated(Article article) {
		articleDAO.saveUpdatedArt(article);
	}
}
