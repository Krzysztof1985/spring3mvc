package pl.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "Article")
public class Article implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9140218413216351022L;
	@Id
	@GeneratedValue
	@Column(name = "article_id")
	private Long articleId;

	@Size(min = 3, max = 20, message = "Za krotka nazwa")
	@NotNull(message = "Nazwa artykulu nie moze byc pusta")
	@Column(name = "article_name")
	private String articleName;

	
	@Size(min=3,max=40,message="ZA krotki opis artykulu")
	@NotNull(message = "opis artykulu nie moze byc pusty")
	@Email(message="Nieprawidlowy e-mail!")
	@Column(name = "article_desc")
	private String articleDesc;

	@Column(name = "date_added")
	private Date addedDate;

	public Long getArticleId() {
		return articleId;
	}

	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}

	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	public String getArticleDesc() {
		return articleDesc;
	}

	public void setArticleDesc(String articleDesc) {
		this.articleDesc = articleDesc;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	@Override
	public String toString() {
		return "Article [articleId=" + articleId + ", articleName=" + articleName + ", articleDesc=" + articleDesc + ", addedDate="
				+ addedDate + "]";
	}
}
